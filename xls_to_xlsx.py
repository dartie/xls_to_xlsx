# -*- coding: utf-8 -*-
#
# Author:   Dario Necco
# Company:  Dartie
#
# This script allows to convert xls files to xlsx
import locale
import os
import sys
import argparse
from argparse import ArgumentParser, RawTextHelpFormatter
import logging
import inspect
import time
import shutil
import io
import re
import subprocess
from subprocess import Popen, PIPE
from sys import platform as _platform
import traceback
import pprint
from distutils import dir_util
from xml.dom import minidom
import datetime
import win32com.client as win32
import threading
import pythoncom
# import chardet  # uncomment only if necessary, it requires installation

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)

if sys.version[0] == '2':
    from imp import reload

    reload(sys)
    sys.setdefaultencoding("utf-8")

# import Colorama
try:
    from colorama import init, Fore, Back, Style
    init()
except ImportError:
    print('Colorama not imported')

locale.setlocale(locale.LC_ALL, 'C')  # set locale

# set version and author  
__version__ = '1.1'  
intern_version = '0002'

# I obtain the app directory
if getattr(sys, 'frozen', False):
    # frozen
    dirapp = os.path.dirname(sys.executable)
    dirapp_bundle = sys._MEIPASS
    executable_name = os.path.basename(sys.executable)
else:
    # unfrozen
    dirapp = os.path.dirname(os.path.realpath(__file__))
    dirapp_bundle = dirapp
    executable_name = os.path.basename(__file__)

##############################################################################################
# DEBUG
this_scriptFile = inspect.getfile(inspect.currentframe())
this_scriptFile_filename = os.path.basename(this_scriptFile)
this_scriptFile_filename_noext, ext = os.path.splitext(this_scriptFile_filename)

# logging.basicConfig(filename=this_scriptFile_filename_noext + '.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')  # uncomment for Logging

print('Working dir: \"' + dirapp + '\"')
welcome_text = '{char1} {appname} v.{version} {char2}'.format(char1='-' * 5, 
                                                              appname=os.path.splitext(os.path.basename(__file__))[0], 
                                                              version=__version__, char2='-' * 5)
print(welcome_text)
logging.info(welcome_text)


def print_error(text):
    text_colored = '{indent}{fontstyle}{text}{reset}'.format(indent='\n', fontstyle=Fore.RED + Style.BRIGHT, text=text, reset=Style.RESET_ALL)
    print(text_colored)


def print_folder(text):
    text_colored = '{indent}{fontstyle}{text}{reset}'.format(indent='\n', fontstyle=Fore.CYAN + Style.BRIGHT, text=text, reset=Style.RESET_ALL)
    print(text_colored)


def print_ok(text):
    text_colored = '{indent}{fontstyle}{text}{reset}'.format(indent='\n', fontstyle=Fore.GREEN + Style.BRIGHT, text=text, reset=Style.RESET_ALL)
    print(text_colored)

    
def print_step(text):
    text_colored = '{indent}{fontstyle}{text}{reset}'.format(indent='\t', fontstyle=Fore.MAGENTA + Style.BRIGHT, text=text, reset=Style.RESET_ALL)
    print(text_colored)

    
def convert_xlx(fname, overwrite=True, fnameout=None):
    if fnameout is None:
        fnameout = fname + "x"
        
    if overwrite and os.path.exists(fnameout):
        try:
            os.remove(fnameout)
        except:
            print('\tI could not remove file "{}". Please answer Yes or No in the Excel prompt'.format(fnameout))
            pass  # Excel will prompt the Message Dialog

    pythoncom.CoInitialize()
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    wb = excel.Workbooks.Open(fname)

    wb.SaveAs(fnameout, FileFormat = 51)     #FileFormat = 51 is for .xlsx extension
    wb.Close()                               #FileFormat = 56 is for .xls extension
    excel.Application.Quit()

    
def get_filetimestamp_now():
    date_time = datetime.datetime.now()  # get datatime info
    dated_text = '%s' % (date_time.day)  # get datatime day in dated
    datem_text = '%s' % (date_time.month)  # get datatime month in datem
    datey_text = '%s' % (date_time.year)  # get datatime year in datey
    date_text = dated_text.rjust(2, '0') + '-' + datem_text.rjust(2, '0') + '-' + datey_text.rjust(4, '0')  # I adjust the date string filling the data and having dd-mm-yyyy
    time_text = date_time.strftime("%X")  # I get the time hh:mm:ss
    file_timestamp = datey_text.rjust(4, '0') + datem_text.rjust(2, '0') + dated_text.rjust(2, '0') + time_text.replace(':', '')

    return file_timestamp


def remove_suffix(text, suffix):
   if text.endswith(suffix):
       return text[:len(text)-len(suffix)]
   return text #or whatever


def zip_no_tree(filenamezip, list_files_to_zip, path_zip=os.path.dirname(os.path.realpath(__file__)), path_list_files=''):
    import zipfile

    zip = zipfile.ZipFile(os.path.join(path_zip, filenamezip), 'w')

    for file in list_files_to_zip:
        if os.path.dirname(os.path.realpath(__file__)) != path_list_files:
            # Copy temp files
            shutil.copy(os.path.join(path_list_files, file), os.path.join(os.path.dirname(os.path.realpath(__file__)), file))

        try: # Create zip
            zip.write(file)
        except:
            print_error('[ERROR] : %s does not exist' %(file))

        if os.path.dirname(os.path.realpath(__file__)) != path_list_files:
            # Remove temp file
            os.remove(os.path.join(os.path.dirname(os.path.realpath(__file__)), file))

    # Close Stream zip
    zip.close()
    # print('\n\n')
    # print('"%s"\npackage created!' %(filenamezip))


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
This tool allows to convert xls files to xlsx format.
The input ("-i" option) can be a folder where all excel files will be processed or a single file.

    """)

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help=argparse.SUPPRESS)
                        # help='Increase output verbosity. Output files don\'t overwrite original ones'

    parser.add_argument("-i", "--input", dest="input_path", default=os.getcwd(),
                        help="Path for input xls files")

    parser.add_argument("-op", "--output", dest="output_path", default='out',
                        help="Path for xlsx converted")

    parser.add_argument("-po", "--prompt-overwrite", dest="prompt_overwrite", default=False, action='store_true',
                        help="if flagged, ask before overwrite any eventual existing output file.")

    parser.add_argument("-r", "--recursive", dest="recursive", default=False, action='store_true',
                        help="Process all subfolders")
    
    # parser.add_argument("-r", "--remove-xlx", dest="remove_xlx", default=False, action='store_true'
    parser.add_argument("-k", "--keep-xlx", dest="keep_xlx", default=False, action='store_true',
                        help="Flag for keeping a copy of the input file. In any case, a zip backup is performed!")
                        # action='store_true'
                        # nargs="+",
                        # nargs='?',  # optional argument
                        # default=""
                        # type=int
                        # choices=[]

    # parser.add_argument("source_file",
                        # help="source file must be analyzed")

    args = parser.parse_args()  # it returns input as variables (args.dest)
    
    #add custom args
    setattr(args, 'overwrite', True if not args.prompt_overwrite else False) 
    
    # end check args

    return args


def print_(xlx_fullpathname, t2):
    print(xlx_fullpathname, t2)


def main(args=None):
    if args is None:
        args = check_args()
    
    # get real path
    args.input_path = os.path.realpath(args.input_path)

    if not os.path.exists(args.input_path):
        print_error('{} does not exist'.format(args.input_path))
        sys.exit(1)

    if os.path.isfile(args.input_path):
        mode = 'file'
        xlx_files = [os.path.basename(args.input_path)]
        xlx_files_fullpath = [args.input_path]
        folder_to_process = os.path.dirname(os.path.realpath(__file__))
        element_to_print = args.input_path
    else:
        mode = 'folder'
        folder_to_process = args.input_path
        element_to_print = folder_to_process
        
        # get file list to process
        xlx_files = [x for x in os.listdir(folder_to_process) if x.endswith('.xls')]
        xlx_files_fullpath = [os.path.join(folder_to_process, x) for x in xlx_files]  # get all files into fullpathname
    
    print_folder('# Processing {} : "{}"'.format(mode, element_to_print))
    
    # check if xlx_files_fullpath has len = 0
    if len(xlx_files_fullpath) == 0:
        print_ok('No files to process')
        return  # continue if in a loop
    
    # Backup
    zipfilename = get_filetimestamp_now() + '_Backup.zip'
    zip_no_tree(zipfilename, xlx_files, path_zip=os.getcwd(), path_list_files=folder_to_process)
    print_step('Backup performed: "{}"'.format(zipfilename))
        
    print_step('Starting conversion')    
    procfile_index = 0
    for xlx_fullpathname in xlx_files_fullpath:
        procfile_index += 1
        if not xlx_fullpathname.endswith('.xls'):
            sys.stdout.write('\r\tFile "{}" has not a format ".xls"'.format(os.path.basename(xlx_fullpathname)))
            sys.stdout.write("\033[K") #clear line
            sys.stdout.write("\033[F") #back to previous line
            sys.stdout.write('\r')
            return
        # xlx_fullpathname = os.path.join(folder_to_process, xlxf)

        # print('\t{}/{} - {}'.format(procfile_index, len(xlx_files_fullpath), os.path.basename(xlx_fullpathname)), '\r')
        sys.stdout.write('\r\t{}/{} - {}'.format(procfile_index, len(xlx_files_fullpath), os.path.basename(xlx_fullpathname)))
        sys.stdout.write("\033[K") #clear line
        sys.stdout.write("\033[F") #back to previous line
        
        convert_xlx(xlx_fullpathname, args.overwrite)
        # t = threading.Thread(target=convert_xlx, args=(xlx_fullpathname, args.overwrite))
        # t.start()  # start the thread
        
        if not args.keep_xlx:
            try:
                pass
                os.remove(xlx_fullpathname)
            except:
                if args.debug:
                    print('[WARNING] : file {} could not be deleted'.format(xlx_fullpathname))

    #t.join()  # the main thread waits for all other threads
    sys.stdout.write('\r')
    print_step('Conversion {} "{}" completed'.format(mode, folder_to_process))
    
    print_ok('All done ! :) ')

    return args


if __name__ == '__main__':
    try:
        start = time.time()
        args = main(args=None)
        end = time.time()
        #if args.debug:
            #print(end - start)
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

# TODO : add recursive mode -r - handle relative paths
# TODO : apply multithread